import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {

		Scanner reader = new Scanner(System.in);


		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

		int MoveCounter=0;
		int currentplyr=0;

		while(MoveCounter<9){
			System.out.print("Player "+(currentplyr+1)+ " enter row number:");
			int row = reader.nextInt();
			System.out.print("Player "+(currentplyr+1)+ " enter column number:");
			int col = reader.nextInt();

			if (row>0 &&row<4 && col>0 && col<4 && board[row - 1][col - 1]== ' ') {
				if (currentplyr == 0)
					board[row - 1][col - 1] = 'X';
				else
					board[row - 1][col - 1] = 'O';
					printBoard(board);

					boolean win =checkboard(board,row - 1, col - 1);
					if(win) {
						System.out.println("Player " + (currentplyr + 1) + " has won the game.");

						break;
					}

				currentplyr=(currentplyr+1)%2;
				printBoard(board);
				MoveCounter++;
			}
			else{
				System.out.println("It is not a valit location.Try another location.");
			}


		}
		if(MoveCounter==9){
			System.out.println("It is a draw");
		}
			reader.close();
	}

	public static boolean checkboard(char[][] board, int row, int col) {
		char symbol = board[row][col];
		boolean win = true;
		//row
		for(int i=0;i<3;i++){
			if(symbol !=board[row][i]){
				win =false;

			}
		}
		if(win)
			return true;

		win=true;
		//col
		for(int j=0;j<3;j++){
			if(symbol!=board[j][col]){
				win=false;

			}
		}
		if(win)
			return true;



		//top lef to right

		if(row==col){
			win=true;
			for(int i=0, j=0; i<3;i++,j++){
				if(symbol!=board[i][j]){
					win=false;
				}
			}
		}
		if(win)
			return  true;
		//bot left to right
		if(row+col==2){
			win=true;
			for(int i=2,j=0;j<3;i--,j++){
				if(symbol!=board[i][j]){
					win=false;
				}
			}
			if(win)
				return true;
		}




		return win;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}